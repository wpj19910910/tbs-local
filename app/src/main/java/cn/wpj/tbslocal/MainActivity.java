package cn.wpj.tbslocal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tencent.smtt.export.external.extension.interfaces.IX5WebChromeClientExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewExtension;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.MediaAccessPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private static final String URL = "http://debugtbs.qq.com";

    X5WebView wv;
    private long exitTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wv = findViewById(R.id.x5);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                Log.i("X5", "LoadUrl:" + view.getUrl());
                // 忽略SSL证书校验
                handler.proceed();
            }
        });
        wv.setWebChromeClientExtension(new IX5WebChromeClientExtension() {
            @Override
            public Object getX5WebChromeClientInstance() {
                return null;
            }

            @Override
            public View getVideoLoadingProgressView() {
                return null;
            }

            @Override
            public void onAllMetaDataFinished(IX5WebViewExtension
                                                      ix5WebViewExtension, HashMap<String, String> hashMap) {

            }

            @Override
            public void onBackforwardFinished(int i) {

            }

            @Override
            public void onHitTestResultForPluginFinished(IX5WebViewExtension
                                                                 ix5WebViewExtension, IX5WebViewBase.HitTestResult hitTestResult, Bundle bundle) {

            }

            @Override
            public void onHitTestResultFinished(IX5WebViewExtension
                                                        ix5WebViewExtension, IX5WebViewBase.HitTestResult hitTestResult) {

            }

            @Override
            public void onPromptScaleSaved(IX5WebViewExtension ix5WebViewExtension) {

            }

            @Override
            public void onPromptNotScalable(IX5WebViewExtension ix5WebViewExtension) {

            }

            @Override
            public boolean onAddFavorite(IX5WebViewExtension ix5WebViewExtension, String s, String
                    s1, JsResult jsResult) {
                return false;
            }

            @Override
            public void onPrepareX5ReadPageDataFinished(IX5WebViewExtension
                                                                ix5WebViewExtension, HashMap<String, String> hashMap) {

            }

            @Override
            public boolean onSavePassword(String s, String s1, String s2, boolean b, Message message) {
                return false;
            }

            @Override
            public boolean onSavePassword(android.webkit.ValueCallback<String> valueCallback, String
                    s, String s1, String s2, String s3, String s4, boolean b) {
                return false;
            }

            @Override
            public void onX5ReadModeAvailableChecked(HashMap<String, String> hashMap) {

            }

            @Override
            public void addFlashView(View view, ViewGroup.LayoutParams layoutParams) {

            }

            @Override
            public void h5videoRequestFullScreen(String s) {

            }

            @Override
            public void h5videoExitFullScreen(String s) {

            }

            @Override
            public void requestFullScreenFlash() {

            }

            @Override
            public void exitFullScreenFlash() {

            }

            @Override
            public void jsRequestFullScreen() {

            }

            @Override
            public void jsExitFullScreen() {

            }

            @Override
            public void acquireWakeLock() {

            }

            @Override
            public void releaseWakeLock() {

            }

            @Override
            public Context getApplicationContex() {
                return null;
            }

            @Override
            public boolean onPageNotResponding(Runnable runnable) {
                return false;
            }

            @Override
            public Object onMiscCallBack(String s, Bundle bundle) {
                return null;
            }

            @Override
            public void openFileChooser(android.webkit.ValueCallback<Uri[]> valueCallback, String
                    s, String s1) {

            }

            @Override
            public void onPrintPage() {

            }

            @Override
            public void onColorModeChanged(long l) {

            }

            @Override
            public boolean onPermissionRequest(String s, long l, MediaAccessPermissionsCallback
                    mediaAccessPermissionsCallback) {
                Log.i("onPermissionRequest", "s:" + s);
                long allowed = 0;
                allowed = allowed | MediaAccessPermissionsCallback.ALLOW_AUDIO_CAPTURE | MediaAccessPermissionsCallback.ALLOW_VIDEO_CAPTURE;
                boolean retain = true;
                mediaAccessPermissionsCallback.invoke(s, allowed, retain);
                return true;
            }
        });
        wv.loadUrl(URL);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (wv != null && wv.canGoBack()) {
                wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                wv.goBack();
                return true;
            }
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(MainActivity.this, "再按一次退出", Toast.LENGTH_LONG).show();
                exitTime = System.currentTimeMillis();
            } else {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
