package cn.wpj.tbslocal;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsListener;

import cn.wpj.tbslocal.permissions.RxPermissions;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        checkPermissions();
    }

    private void checkPermissions() {
        RxPermissions rxPermissions = new RxPermissions(SplashActivity.this);
        rxPermissions.request(Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    initTBS();
                } else {
                    Toast.makeText(SplashActivity.this, "缺少必要权限将导致功能异常，请授权", Toast.LENGTH_SHORT).show();
                    checkPermissions();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });
    }

    public void initTBS() {
        Log.i("QbSdk","开始初始化");
        QbSdk.preinstallStaticTbs(getApplicationContext());
        QbSdk.setTbsListener(new TbsListener() {
            @Override
            public void onDownloadFinish(int i) {
                // 下载结束时的状态，下载成功时errorCode为100,其他均为失败，外部不需要关注具体的失败原因
                Log.i("QbSdk", "onDownloadFinish -->下载X5内核完成：" + i);
                Log.i("QbSdk", "onDownloadFinish---是否可以加载X5内核: " + QbSdk.canLoadX5(SplashActivity.this));

            }

            @Override
            public void onInstallFinish(int i) {
                // 安装结束时的状态，安装成功时errorCode为200,其他均为失败，外部不需要关注具体的失败原因
                Log.i("QbSdk", "onInstallFinish -->安装X5内核进度：" + i);
                Log.i("QbSdk", "onInstallFinish---是否可以加载X5内核: " + QbSdk.canLoadX5(SplashActivity.this));
            }

            @Override
            public void onDownloadProgress(int i) {
                // 下载过程的通知，提供当前下载进度[0-100]
                Log.i("QbSdk", "onDownloadProgress -->下载X5内核进度：" + i);
                Log.i("QbSdk", "onDownloadProgress---是否可以加载X5内核: " + QbSdk.canLoadX5(SplashActivity.this));
            }
        });
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {
                boolean flag = QbSdk.canLoadX5(SplashActivity.this);
                // 内核初始化完毕
                Log.i("QbSdk", "onCoreInitFinished ");
                Log.i("QbSdk", "onCoreInitFinished---是否可以加载X5内核: " + flag);
                if(!flag) {
                    Toast.makeText(SplashActivity.this,"X5内核加载失败，重新加载。。", Toast.LENGTH_SHORT).show();
                    initTBS();
                }
            }

            @Override
            public void onViewInitFinished(boolean arg0) {
               boolean flag = QbSdk.canLoadX5(SplashActivity.this);
                // x5內核初始化完成的回调，true表x5内核加载成功，否则表加载失败，会自动切换到系统内核。
                Log.i("QbSdk", " 内核加载：" + arg0);
                Log.i("QbSdk", "onViewInitFinished---是否可以加载X5内核: " + flag);
                if(flag) {
                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                }else {
                    Toast.makeText(SplashActivity.this,"X5内核加载失败，重新加载。。", Toast.LENGTH_SHORT).show();
                    initTBS();
                }
            }
        };
        // x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
    }
}
