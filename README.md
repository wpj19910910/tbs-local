# TbsLocal
TBS X5内核离线版，无网络情况可加载成功，可运行音视频webrtc

1.将tbs_sdk_thirdapp_v3.5.0.1063_43500_staticwithdownload_withoutGame_obfs_20171011_195714.jar放入lib
2.build.gradle加入配置：
Ⅰ：注意！这里所有配置的版本都要有对应版本的so引入，否则初始化的时候无法引用到so包
ndk {
    abiFilters "armeabi",/*"armeabi-v7a","x86"*/
}
Ⅱ:
sourceSets {
  main {
    jniLibs.srcDirs = ['libs']
    java.srcDirs = ['src/main/java']
  }
}
Ⅲ：
implementation fileTree(include: ['*.jar'], dir: 'libs')
3.下载内核，使用http://debugtbs.qq.com中的工具下载
4.将对应的tbs文件解压，将其中的libs下的arm文件夹中的so，assets\webkit中的所有文件，全部导出到一个文件夹，然后改名，可使用下面cmd命令
for /F %i in ('dir /A:-D /B') do move %i "libtbs.%i.so"
5.将改名的后的文件导入对应的Lib下的arm文件夹中
6.初始化执行SplashActivity中的initTBS()方法

备注：
查询设备的abi版本：adb shell getprop ro.product.cpu.abi
·只适配armeabi的APP可以跑在armeabi,x86,x86_64,armeabi-v7a,arm64-v8上
·只适配armeabi-v7a可以运行在armeabi-v7a和arm64-v8a
·只适配arm64-v8a 可以运行在arm64-v8a上

参考资料：
https://www.jianshu.com/p/45c377d6d410
https://blog.csdn.net/Mr_JingFu/article/details/111864734
